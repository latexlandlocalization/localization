<div align="center">

 <a href="https://gitlab.com/latexlandlocalization/localization/-/blob/main/LICENSE">
   <img src="https://img.shields.io/github/license/whilein/nmslib">
 </a>

 ***
Данный репозиторий используется для мульти-язяковой системы проекта **LatexLand (mc.latexland.ru)**
</div>

---
## Обратная связь
Если у Вас есть какие-то предложения или желание в помощи по переводу, 
то можете обратиться к нам через указанные контакты:

* **[ВКонтакте](https://vk.me/latexland)**
